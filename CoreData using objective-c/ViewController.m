//
//  ViewController.m
//  CoreData using objective-c
//
//  Created by OSX on 25/01/17.
//  Copyright © 2017 Ameba. All rights reserved.
//

#import "ViewController.h"
#import "AddRecordsViewController.h"
#import <CoreData/CoreData.h>

@interface ViewController () <UITableViewDelegate, UITableViewDataSource>
{
    NSMutableArray *fetchRecordsArr;
}
@end

@implementation ViewController
@synthesize showRecordsTV;

- (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // Fetch the records from persistant store
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Records"];
    fetchRecordsArr = [[context executeFetchRequest:fetchRequest error:nil] mutableCopy];
    
    [showRecordsTV reloadData];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return fetchRecordsArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"recordsCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    NSManagedObject *records = [fetchRecordsArr objectAtIndex:indexPath.row];
    
    //cell.textLabel.text = [records valueForKey:@"rollNumber"];
    cell.textLabel.text = [NSString stringWithFormat:@"Roll Number:- %@ Name:- %@",[records valueForKey:@"rollNumber"], [records valueForKey:@"name"]];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSManagedObject *managedObject = [fetchRecordsArr objectAtIndex:indexPath.row];
    AddRecordsViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"AddRecordsViewController"];
    controller.managedObject = managedObject;
    [self.navigationController pushViewController:controller animated:YES];
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    NSManagedObjectContext *context = [self managedObjectContext];
    
    // Here i have delete the object from array
    [context deleteObject:[fetchRecordsArr objectAtIndex:indexPath.row]];
    
    NSError *error = nil;
    if (![context save:&error]) {
        NSLog(@"Can't delete %@", [error localizedDescription]);
    }
    
    // delete from tableView
    [fetchRecordsArr removeObjectAtIndex:indexPath.row];
    [showRecordsTV deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)addRecordsAction:(id)sender {
    AddRecordsViewController *recordsController = [self.storyboard instantiateViewControllerWithIdentifier:@"AddRecordsViewController"];
    [self.navigationController pushViewController:recordsController animated:YES];
}

@end
