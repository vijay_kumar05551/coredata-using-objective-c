//
//  ViewController.h
//  CoreData using objective-c
//
//  Created by OSX on 25/01/17.
//  Copyright © 2017 Ameba. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *showRecordsTV;
- (IBAction)addRecordsAction:(id)sender;

@end

