//
//  AddRecordsViewController.h
//  CoreData using objective-c
//
//  Created by OSX on 25/01/17.
//  Copyright © 2017 Ameba. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AddRecordsViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *rollNumberTF;
@property (weak, nonatomic) IBOutlet UITextField *nameTF;
@property (weak, nonatomic) IBOutlet UITextField *departmentTF;
@property (weak, nonatomic) IBOutlet UITextField *isMarriedTF;

@property (strong, nonatomic) NSManagedObject *managedObject;

@end
