//
//  main.m
//  CoreData using objective-c
//
//  Created by OSX on 25/01/17.
//  Copyright © 2017 Ameba. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
