//
//  AddRecordsViewController.m
//  CoreData using objective-c
//
//  Created by OSX on 25/01/17.
//  Copyright © 2017 Ameba. All rights reserved.
//

#import "AddRecordsViewController.h"

@interface AddRecordsViewController ()

@end

@implementation AddRecordsViewController
@synthesize rollNumberTF, nameTF, departmentTF, isMarriedTF;
@synthesize managedObject;

- (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    // for left button item
    UIButton *leftButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 60, 40)];
    [leftButton setTitle:@"Cancel" forState:UIControlStateNormal];
    [leftButton setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    [leftButton addTarget:self action:@selector(leftButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    // for right button item
    UIButton *rightButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 50, 40)];
    [rightButton setTitle:@"Save" forState:UIControlStateNormal];
    [rightButton setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    [rightButton addTarget:self action:@selector(rightButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *leftButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
    UIBarButtonItem *rightButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightButton];
    
    self.navigationItem.leftBarButtonItem = leftButtonItem;
    self.navigationItem.rightBarButtonItem = rightButtonItem;
    
    if (managedObject)
    {
        rollNumberTF.text = [NSString stringWithFormat:@"%@",[managedObject valueForKey:@"rollNumber"]];
        nameTF.text = [managedObject valueForKey:@"name"];
        departmentTF.text = [managedObject valueForKey:@"department"];
        if ([[managedObject valueForKey:@"married"] intValue] == 0)
        {
            isMarriedTF.text = @"No";
        }
        else {
            isMarriedTF.text = @"Yes";
        }
    }
}

- (void) leftButtonAction:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) rightButtonAction:(UIButton *)sender {
    
    if (managedObject)
    {
        [managedObject setValue:[NSNumber numberWithInteger:[rollNumberTF.text integerValue]] forKey:@"rollNumber"];
        [managedObject setValue:nameTF.text forKey:@"name"];
        [managedObject setValue:departmentTF.text forKey:@"department"];
        [managedObject setValue:[NSNumber numberWithBool:[isMarriedTF.text boolValue]] forKey:@"married"];
    }
    
    else
    {
        NSManagedObjectContext *context = [self managedObjectContext];
        //Create a managed object
        NSManagedObject *newRecords = [NSEntityDescription insertNewObjectForEntityForName:@"Records" inManagedObjectContext:context];
        [newRecords setValue:[NSNumber numberWithInteger:[rollNumberTF.text integerValue]] forKey:@"rollNumber"];
        [newRecords setValue:nameTF.text forKey:@"name"];
        [newRecords setValue:departmentTF.text forKey:@"department"];
        [newRecords setValue:[NSNumber numberWithBool:[isMarriedTF.text boolValue]] forKey:@"married"];
        
        NSError *error = nil;
        // Save the object to persistent store
        if (![context save:&error]) {
            NSLog(@"Error %@",[error localizedDescription]);
        }
    }
    
     [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
